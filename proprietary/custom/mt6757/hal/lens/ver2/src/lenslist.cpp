/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/

#include <utils/Log.h>
#include <utils/Errors.h>
#include <string.h>
#include <fcntl.h>
#include <math.h>
#include "MediaTypes.h"
#include "camera_custom_lens.h"
#include "kd_imgsensor.h"

extern PFUNC_GETLENSDEFAULT pDummy_getDefaultData;

#if 1//defined(AK7371AF)
extern PFUNC_GETLENSDEFAULT pAK7371AF_getDefaultData;
extern PFUNC_GETLENSDEFAULT pAK7371AF_MAIN2_getDefaultData;
#endif

#if 1//defined(LC898212XDAF)
extern PFUNC_GETLENSDEFAULT pLC898212XDAF_getDefaultData;
extern PFUNC_GETLENSDEFAULT pLC898212XDAF_BAYER_getDefaultData;
extern PFUNC_GETLENSDEFAULT pLC898212XDAF_MONO_getDefaultData;
#endif

#if 1//defined(BU6424AF)
extern PFUNC_GETLENSDEFAULT pBU6424AF_getDefaultData;
#endif

#if defined(BU6429AF)
extern PFUNC_GETLENSDEFAULT pBU6429AF_getDefaultData;
#endif

#if 1//defined(BU63169AF)
extern PFUNC_GETLENSDEFAULT pBU63169AF_getDefaultData;
#endif

MSDK_LENS_INIT_FUNCTION_STRUCT LensList_main[MAX_NUM_OF_SUPPORT_LENS] =
{
    {DUMMY_SENSOR_ID, DUMMY_LENS_ID, "Dummy", pDummy_getDefaultData},
    #if 1//defined(BU63169AF)
        {IMX386_SENSOR_ID, BU63169AF_LENS_ID, "BU63169AF", pBU63169AF_getDefaultData},
    #endif
    #if 1//defined(AK7371AF)
        {IMX338_SENSOR_ID, AK7371AF_LENS_ID, "AK7371AF", pAK7371AF_getDefaultData},
        //{S5K2L7_SENSOR_ID, AK7371AF_LENS_ID, "AK7371AF", pAK7371AF_getDefaultData},
    #endif
    #if 1//defined(LC898212XDAF)
        {S5K2L7_SENSOR_ID, LC898212XDAF_LENS_ID, "LC898212XDAF_TVC700", pLC898212XDAF_getDefaultData},
        {IMX258_SENSOR_ID, LC898212XDAF_LENS_ID, "LC898212XDAF", pLC898212XDAF_BAYER_getDefaultData},
    #endif
    #if defined(BU6429AF)
        //{IMX386_SENSOR_ID, BU6429AF_LENS_ID, "BU6429AF", pBU6429AF_getDefaultData},
        {S5K2P8_SENSOR_ID, BU6429AF_LENS_ID, "BU6429AF", pBU6429AF_getDefaultData},
    #endif
};
MSDK_LENS_INIT_FUNCTION_STRUCT LensList_sub[MAX_NUM_OF_SUPPORT_LENS] =
{
    {DUMMY_SENSOR_ID, DUMMY_LENS_ID, "Dummy", pDummy_getDefaultData},
    #if 1//defined(BU6424AF)
        {OV8858_SENSOR_ID, BU6424AF_LENS_ID, "BU6424AF", pBU6424AF_getDefaultData},
    #endif
};
MSDK_LENS_INIT_FUNCTION_STRUCT LensList_main2[MAX_NUM_OF_SUPPORT_LENS] =
{
    {DUMMY_SENSOR_ID, DUMMY_LENS_ID, "Dummy", pDummy_getDefaultData},
    #if 1//defined(AK7371AF)
        {S5K3M3_SENSOR_ID, AK7371AF_LENS_ID, "AK7371AF", pAK7371AF_MAIN2_getDefaultData},
    #endif
    #if 1//defined(LC898212XDAF)
        {IMX258_MONO_SENSOR_ID, LC898212XDAF_LENS_ID, "LC898212XDAF_F", pLC898212XDAF_MONO_getDefaultData},
    #endif
};



UINT32 GetLensInitFuncList(PMSDK_LENS_INIT_FUNCTION_STRUCT pLensList, unsigned int a_u4CurrSensorDev)
{
    if(a_u4CurrSensorDev==2) //sub
        memcpy(pLensList, &LensList_sub[0], sizeof(MSDK_LENS_INIT_FUNCTION_STRUCT)* MAX_NUM_OF_SUPPORT_LENS);
    else if(a_u4CurrSensorDev==4) //main 2
        memcpy(pLensList, &LensList_main2[0], sizeof(MSDK_LENS_INIT_FUNCTION_STRUCT)* MAX_NUM_OF_SUPPORT_LENS);
    else  // main or others
        memcpy(pLensList, &LensList_main[0], sizeof(MSDK_LENS_INIT_FUNCTION_STRUCT)* MAX_NUM_OF_SUPPORT_LENS);

    return 0;
}
